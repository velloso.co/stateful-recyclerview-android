package co.velloso.android.statefulrecyclerview

import android.os.Bundle
import android.os.Parcelable
import android.util.SparseArray
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_ID


abstract class StatefulRecyclerAdapter<T, VH : RecyclerView.ViewHolder>(diffCallback: DiffUtil.ItemCallback<T>) : ListAdapter<T, VH>(diffCallback) {

    /**
     * A map with the ViewHolder id as key and the ViewHolder.itemView SparseArray state as value
     */
    private val itemViewStates = mutableMapOf<Long, SparseArray<Parcelable>>()

    final override fun registerAdapterDataObserver(observer: RecyclerView.AdapterDataObserver) {
        super.registerAdapterDataObserver(observer)
    }

    override fun onViewDetachedFromWindow(holder: VH) {
        super.onViewDetachedFromWindow(holder)
        if (holder is StatefulViewHolder && holder.itemId != NO_ID) {
            holder.onSaveItemViewHolderState(itemViewStates.getOrPut(holder.itemId) { SparseArray() })
        }
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        if (holder is StatefulViewHolder && holder.itemId != NO_ID) {
            holder.onRestoreItemViewHolderState(itemViewStates.getOrPut(holder.itemId) { SparseArray() })
        }
    }

    fun onSaveInstanceState(): Parcelable? {
        val state = Bundle()
        itemViewStates.forEach { state.putSparseParcelableArray(it.key.toString(), it.value) }
        return state
    }

    fun onRestoreInstanceState(state: Parcelable?) {
        (state as? Bundle)?.keySet()?.forEach {
            val id = it.toLongOrNull() ?: return@forEach
            (state as? Bundle)?.getSparseParcelableArray<Parcelable>(id.toString())?.let { itemState ->
                itemViewStates[id] = itemState
            }
        }
    }
}