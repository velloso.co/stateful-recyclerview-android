package co.velloso.android.statefulrecyclerview

import android.os.Parcelable
import android.util.SparseArray
import android.view.ViewGroup
import androidx.annotation.LayoutRes

open class StatefulViewHolder(parent: ViewGroup, @LayoutRes layoutId: Int) : BindableViewHolder(parent, layoutId) {

    /**
     *
     * This is called after super RecyclerView.Adapter#onViewDetachedFromWindow().
     *
     * Override this if it's not necessary to save all the view state, for example, if only scroll
     * views need their state saved, override this with:
     * ```
     *     itemView.findViewById(R.id.myScrollView).saveHierarchyState(state)
     * ```
     */
    open fun onSaveItemViewHolderState(state: SparseArray<Parcelable>) {
        itemView.saveHierarchyState(state)
    }

    /**
     *
     * This is called after the RecyclerView.Adapter#onBindViewHolder() so if the adapter
     * data changes while the holder is not visible, the itemView state saved TODO has to be
     * invalidated.
     *
     * Override this if it's not necessary to restore all the view state, for example, if only a
     * scroll view need its state saved, override this with:
     * ```
     *     itemView.findViewById(R.id.myScrollView).restoreHierarchyState(state)
     * ```
     */
    open fun onRestoreItemViewHolderState(state: SparseArray<Parcelable>) {
        itemView.restoreHierarchyState(state)
    }
}