package co.velloso.android.statefulrecyclerview

import android.content.Context
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.SparseArray
import androidx.core.view.children
import androidx.customview.view.AbsSavedState
import androidx.recyclerview.widget.RecyclerView


class StatefulRecyclerView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int = 0

) : RecyclerView(context, attrs, defStyle) {

    override fun onSaveInstanceState(): Parcelable? {

        // super will return with the LayoutManager state
        val state = SavedState(super.onSaveInstanceState()!!)

        // Get adapter states (views out of window)
        val adapter = this.adapter
        if (adapter?.hasStableIds() != true) return state

        val adapterState = if (adapter is StatefulRecyclerAdapter<*, *>) {
            adapter.onSaveInstanceState() as Bundle
        } else Bundle()

        // Add to adapter states the states that are currently on the recycler view,
        // overriding any state that are possible not up-to-date
        children.forEach {
            val itemId = getChildItemId(it)
            if (itemId != NO_ID) {
                val childState = SparseArray<Parcelable>()
                it.saveHierarchyState(childState)
                adapterState.putSparseParcelableArray(itemId.toString(), childState)
            }
        }

        // finally add adapter states to SavedState state
        state.adapterState = adapterState

        return state
    }

    override fun onRestoreInstanceState(state: Parcelable?) {

        if (state !is SavedState) {
            super.onRestoreInstanceState(state)
            return
        }

        // super will restore layout manager
        super.onRestoreInstanceState(state.superState)

        // restore adapter
        val adapter = this.adapter
        if (adapter != null && state.adapterState != null &&
                adapter is StatefulRecyclerAdapter<*, *>
        ) {
            adapter.onRestoreInstanceState(state.adapterState)
        }
    }

    class SavedState : AbsSavedState {

        internal var adapterState: Parcelable? = null

        constructor(parcel: Parcel) : super(parcel)

        internal constructor(input: Parcel, loader: ClassLoader?) : super(input, loader) {
            adapterState = input.readParcelable(loader ?: Adapter::class.java.classLoader)
        }

        internal constructor(superState: Parcelable) : super(superState)

        override fun writeToParcel(dest: Parcel, flags: Int) {
            super.writeToParcel(dest, flags)
            dest.writeParcelable(adapterState, 0)
        }

        internal fun copyFrom(other: SavedState) {
            adapterState = other.adapterState
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object {

            @JvmField
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.ClassLoaderCreator<SavedState> {
                override fun createFromParcel(input: Parcel, loader: ClassLoader): SavedState {
                    return SavedState(input, loader)
                }

                override fun createFromParcel(input: Parcel): SavedState {
                    return SavedState(input, null)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }
}